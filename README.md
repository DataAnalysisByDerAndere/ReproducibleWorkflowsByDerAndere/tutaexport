# TutaExport
tutaexport is a tool to converted exported contacts from tutanota into a valid vCard file for migration of address books to other organizer software / Email clients like Thunderbird.

## Installation
1. Install a Python 3.7 or Python 3.8 interpreter that comes with all distributions of Python. 
For Windows, I recommend CPython that is part of the [reference Python 
distribution by the Python Foundation](https://www.python.org/downloads/) 
(an executable installer is provided for download). Run the installer by double-
clicking the executable file you downloaded. Follow the instruction on the 
screen and when the installer wizard dialog gives the option to "Install Python launcher for Windows"
and "Install Python in Path", select both options. 

2. Installation of external Python packages except for pyserial is usually done 
automatically during installation of tutaexport with pip. 
If you want to install the dependencies seperately, you can use the tool pip by 
the Python Packaging Authority (PyPa) which is included in the Python 3.7 or Python 3.8
distribution of the Python Foundation (if you don't have pip installed, follow 
[the instructions](https://pip.pypa.io/en/stable/installing/) ). 
Then navigate to the Powershell_ISE.exe and right-click the executable 
Powershell_ISE.exe. In the context menu that appears, left-click "Run as Administrator". 
To install the development version of tutaexport, type the following in the command prompt and confirm by pressing the 
enter-key:
```
py -3.8 -m pip -r install https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaexport/-/archive/main/tutaexport-main.tar.gz
```

Alternatively, install the latest tagged release of tutaexport [tutaexport](https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaexport/-/releases).
as follows (replace the `<tag>` by the desired version number (tag)):
```
py -3.8 -m pip install https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaexport/-/archive/<tag>/tutaexport-<tag>.tar.gz
```

Or download the distribution package attached to the release notes (preferably 
the binary wheel) and install from the downloaded archive or wheel:
```
py -3.8 -m pip install "<Path\to\downloaded\folder>"
```

## Usage
1. In tutanota: Manually format the addresses so that each address has one of the following formats: 
Either
```
Street
Locality
Region
Postal Code
Country
```
or
```
Street
Locality
Postal Code
Country
```

or
```
Street
Locality
Region
Postal Code
```
or
```
Street
Locality
Postal Code
```

Use dummy values (e.g. `0000` for Postal code, `NA` for other address components) as placeholders for missing values of partially existing address information.

2. In tutanota: Click on '...' next to 'All Contacts'
3. In tutanota: Click on 'export vCard'. Enter a file name (e.g. tutanotacontacts) and select the target directory. Click on 'Save'
4. Navigate to the Powershell_ISE.exe and double-click the executable to run it.
5. In the command prompt, type the following and confirm by pressing the enter-key:

- Method A:
```
tutaexport_execute --infile="<Path\to\file\tutanotacontacts.vcf>" --outfile="<Path\to\output\file\vCardv3.vcf>"
```

- Method B:
```
py -3.8 tutaexport --infile="<Path\to\file\tutanotacontacts.vcf>" --outfile="<Path\to\output\file\vCardv3.vcf>"
```

- Method C:
```
cd "<Path\to\directory\of\tutanotacontacts\>"
tutaexport_execute --infile="<tutanotacontacts.vcf>" --outfile="<Path\to\output\file\vCardv3.vcf>"
```

## Support
https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaexport/issues

## Contributing
Merge requests (pull requests) targeting the main branch are welcome. For details, see file [CONTRIBUTING.md](./CONRTIBUTING.md).

## Developing TutaExport
Get the source code for tutaexport
by forking tutaexport and cloning the fork using git. If you want to download only
the current files of the selected branch, click the control panel "Download"
(next to the control panel "clone").

### Distribute TutaExport
Update all version numbers and push the changes
Create a tag for the last commit (e.g. tag: 1.0.1, message: "tagged release of tutaexporter v1.0.1" and push the changes
Download https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaexport/-/archive/main/tutaexporter-main.tar.gz and extract the archive.
run powershell.exe and enter the following command to change directory to the parent directory of the setup.py file within tutaexport:

```
cd "Path\to\parentdirectory\tutaexport"
```

To create a wheel (.whl file) in the subdirectory called "dis", enter
```
py -3.8 setup.py bdist_wheel
```
Then, log in to gitlab.com
and go to [gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/TutaExport -> Deployments -> Releases](https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaexport/-/releases)

Left-click "New Release". Release name: v1.0.1, release notes: tagged release of tutaexport 1.0.1 and attach the wheel (*.whl) from the step above.
Left-click "OK"


## License information
```
tutaexport
Copyright 2022 DerAndere

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
