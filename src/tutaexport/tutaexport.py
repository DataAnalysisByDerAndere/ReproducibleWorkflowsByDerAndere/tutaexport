# SPDX-License-Identifier: Apache-2.0

"""
tutaexport
Copyright 2022 DerAndere

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

"""
@file tutaexport/tutaexport.py
"""

import sys
import pathlib
import argparse
import re

def convert_to_vCard3(text):
    processed = text
    # replace multi-line ADR value by single line
    for i in range(6):
        processed = re.sub(r"(ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:.+)\n ", r"\1", processed)
    # replace up to 6 occurances of \n with ; in lines starting with ADR
    for i in range(6):
        processed = re.sub(r"(ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:.+)\\n", r"\1;", processed)
    # where last address component contains digits, add ; for missing Country
    processed = re.sub(r"(ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:.+\d[^;]*)(\n[^ ])", r"\1;\2", processed) 
    # Add ; for missing Region info if only 4 Adress components were specified in the original
    processed = re.sub(r"(ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:[^;]+;[^;]+;)([^;]+;[^;]*\n[^ ])", r"\1;\2", processed)  
    # Add ;; at start of ADR value where 5 components are present (assume PO-Box and Extended-Address were not specified)
    processed = re.sub(r"(ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:)([^;]+;[^;]+;[^;]*;[^;]+;[^;]*\n[^ ])", r"\1;;\2", processed)
    # Add ; at start of ADR value where 6 components are present (assume PO-Box was not specified)
    processed = re.sub(r"(ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:)([^;]+;[^;]+;[^;]+;[^;]*;[^;]+;[^;]*\n[^ ])", r"\1;\2", processed)
    return processed
 
def execute(infile=None,
            outfile=None):
    """
    entrypoint to run the conversion from the python shell:
    ```(py)
    tutaexport.execute("infile="<Path\to\file\tutanota.vcf>", outfile="<Path\to\output\file\vCardv3.vcf>")
    ```
    """
    with infile.open('r') as fin, outfile.open('w') as fout:
        original = fin.read()
        fout.write(convert_to_vCard3(original))

def get_arguments(parser):
    """ Get the argument parser for this module
    Useful if you want to use this module as a component of another CLI program
    and want to add its arguments.
    :param parser: A parser to add arguments to.
    :returns argparse.ArgumentParser: The parser with arguments added.
    """
    parser.add_argument(
        '-i', '--infile',
        type=pathlib.Path,
        required=True,
        help='The input file to convert')
    parser.add_argument(
        '-o', '--outfile',
        type=pathlib.Path,
        required=True,
        help='specify output file')
    return parser

def main():
    """
    Handler for command line invocation to fix vCard  (.vcf) files exported from tutanota.
    @param argv: The arguments the program was invoked with; this is usually
                 :py:attr:`sys.argv` but if you want to override that you can.
    @returns int: A success or failure value suitable for use as a shell
                  return code passed to :py:meth:`sys.exit` (0 means success,
                  anything else is a kind of failure).
    """
    parser = argparse.ArgumentParser(prog='tutaexport_execute',
                                     description='Run tutaexport')
    parser = get_arguments(parser)
    args = parser.parse_args()
    execute(infile=args.infile, outfile=args.outfile)
    return 0

if __name__ == '__main__':
    sys.exit(main())
